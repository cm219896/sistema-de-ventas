<style type = "text / css" media = "screen">
	cuerpo{
		imagen de fondo: url (img / login.png);
		tamaño de fondo: cubierta;
        -moz-background-size: portada;
        -webkit-background-size: portada;
        -o-background-size: cover;
	}
</style>

<! DOCTYPE html>
<html>
<cabeza>
	<title> Sistema de Control CarlosJimenez </title>

	<! - bootstrap ->
	<link rel = "stylesheet" href = "assests / bootstrap / css / bootstrap.min.css">
	<! - tema bootstrap ->
	<link rel = "stylesheet" href = "assests / bootstrap / css / bootstrap-theme.min.css">
	<! - fuente impresionante ->
	<link rel = "stylesheet" href = "assests / font-awesome / css / font-awesome.min.css">

  <! - CSS personalizado ->
  <link rel = "stylesheet" href = "custom / css / custom.css">	

  <! - jquery ->
	<script src = "assests / jquery / jquery.min.js"> </script>
  <! - jquery ui ->  
  <link rel = "stylesheet" href = "assests / jquery-ui / jquery-ui.min.css">
  <script src = "assests / jquery-ui / jquery-ui.min.js"> </script>

  <! - bootstrap js ->
	<script src = "assests / bootstrap / js / bootstrap.min.js"> </script>
</head>
<cuerpo>
	<div class = "contenedor">
		<div class = "fila vertical">
			<div class = "col-md-5 col-md-offset-4">
				<div class = "panel panel-primary">
					<div class = "panel-header">
						<h3 class = "panel-title"> Inicio de sesión </h3>
					</div>
					<div class = "panel-body">

					<! - <div class = "mensajes">
							# <? php #if ($ errores) {
							# foreach ($ errores como $ clave => $ valor) {
							# echo '<div class = "alerta alerta-advertencia" role = "alerta">
							# <i class = "glyphicon glyphicon-exclamation-sign"> </i>
							#		'.$value.'</div>';										
							#		}
							#	} ?>
						</div>-->

						<form class="form-horizontal" action="php_action/validar_usuario.php" method="post" id="loginForm">
							<fieldset>

							  <div class="form-group">
									<label for="username" class="col-sm-3 control-label">Usuario</label>
									<div class="col-sm-9">
									  <input type="text" class="form-control" id="username" name="username" placeholder="Nombre de usuario" autocomplete="off" required />
									</div>
								</div>

								<div class="form-group">
									<label for="password" class="col-sm-3 control-label">Contraseña</label>
									<div class="col-sm-9">
									  <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" autocomplete="off" required />
									</div>
								</div>	

								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
									  <button type="submit" class="btn btn-success"> <i class="glyphicon glyphicon-log-in"></i> Ingresar</button>
									</div>
								</div>

							</fieldset>
						</form>
					</div>
					<!-- panel-body -->
				</div>
				<!-- /panel -->
			</div>
			<!-- /col-md-4 -->
		</div>
		<!-- /row -->
	</div>
	<!-- container -->	
</body>
</html>
